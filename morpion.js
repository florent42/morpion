class game {

    constructor() {
        this.i = 0;
        this.cases = document.querySelectorAll('.case')
        this.seconde = 0;
        this.minute = 0;
        this.timeOn = false;
        //variables pour le compteur
        this.moves = 0;
        this.counter = document.querySelector('.moves i');


    }

    playAgain(){
        this.cases.forEach(caseEl => caseEl.innerHTML = '');

        this.moves = 0;
    }

    player() {
        this.symbol = document.querySelector('#player');
        this.symbol.innerHTML = "X";

    }
    tour() {

        this.Time(this.seconde, this.minute);
        this.i++;

        if (!this.isGagner()) {
            if (this.i % 2 === 0) {
                this.symbol.innerHTML = "X"
                return 'O';
            }
            else {
                this.symbol.innerHTML = "O"
                return 'X';
            }
        }


        this.time(this.seconde, this.minute);
    }

    afficherGagnant() {
        if (this.symbol.innerHTML === 'O') {
            alert('X a gagné !')
        }
        else {
            alert('O a gagné !')
        }

        document.querySelector("#startAgainButton").style.display = "initial";
    }

    isGagner() {

        //lignes
        if (this.cases[0].innerHTML !== '' && this.cases[0].innerHTML === this.cases[1].innerHTML && this.cases[1].innerHTML === this.cases[2].innerHTML) {
            this.afficherGagnant(this.cases[0].innerHTML);
        }
        else if (this.cases[3].innerHTML !== '' && this.cases[3].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[5].innerHTML) {
            this.afficherGagnant(this.cases[3].innerHTML);
        }

        else if (this.cases[6].innerHTML !== '' && this.cases[6].innerHTML === this.cases[7].innerHTML && this.cases[7].innerHTML === this.cases[8].innerHTML) {
            this.afficherGagnant(this.cases[6].innerHTML);
        }

        // colonnes
        else if (this.cases[0].innerHTML !== '' && this.cases[0].innerHTML === this.cases[3].innerHTML && this.cases[3].innerHTML === this.cases[6].innerHTML) {
            this.afficherGagnant(this.cases[0].innerHTML);
        }
        else if (this.cases[1].innerHTML !== '' && this.cases[1].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[7].innerHTML) {
            this.afficherGagnant(this.cases[1].innerHTML);
        }
        else if (this.cases[2].innerHTML !== '' && this.cases[2].innerHTML === this.cases[5].innerHTML && this.cases[5].innerHTML === this.cases[8].innerHTML) {
            this.afficherGagnant(this.cases[2].innerHTML);
        }

        //DIAGONALES
        else if (this.cases[0].innerHTML !== '' && this.cases[0].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[8].innerHTML) {
            this.afficherGagnant(this.cases[0].innerHTML);
        }
        else if (this.cases[2].innerHTML !== '' && this.cases[2].innerHTML === this.cases[4].innerHTML && this.cases[4].innerHTML === this.cases[6].innerHTML) {
            this.afficherGagnant(this.cases[2].innerHTML);
        }
    }

    jouer() {
        this.cases.forEach(caseEl => caseEl.addEventListener('click', event => {

            let clicked = event.target;

            if (caseEl.innerHTML === 'O' || caseEl.innerHTML === 'X') {
                return;
            } else {
                clicked.innerHTML = this.tour();
            }
            this.isGagner();
            this.addMove();
        }));

    }

    Time(seconde, minute) {
        if (this.timeOn == false) {
            this.timeOn = true;
            setInterval(function () {
                let TimeShow = document.querySelector('#Time');
                TimeShow.innerHTML = seconde;
                seconde++;
                if (seconde >= 40) {
                    minute++;
                    seconde = 0;
                }
                console.log(seconde);
            }, 1000);
        } else {
            return;
        }
    }
    addMove() {
        this.moves++;
        this.counter.innerHTML = this.moves;
    }
}

let play = new game;
play.jouer();
play.player();




// //let i = 0;

// const cases = document.querySelectorAll ('.case');


// function tour() {

//     i++;
//     return i %  2 === 0 ? 'O' : 'X';

// }
// function afficherGagnant() {
//     alert ( ' a gagné !')
//     cases.forEach(el=>el.innerHTML ='');
// }

// 

// function isGagner() {
//     //lignes
//     if (cases [0].innerHTML !== '' && cases [0].innerHTML === cases[1].innerHTML && cases [1].innerHTML === cases [2].innerHTML){
//     afficherGagnant(cases[0].innerHTML);
// }
//     else if (cases [3].innerHTML !== '' && cases [3].innerHTML === cases[4].innerHTML && cases [4].innerHTML === cases [5].innerHTML) {
//     afficherGagnant(cases[3].innerHTML);
// }

//     else if (cases [6].innerHTML !== '' && cases [6].innerHTML === cases[7].innerHTML && cases [7].innerHTML === cases [8].innerHTML) {
//     afficherGagnant(cases[6].innerHTML);
// }
//     //colonnes

//     else if (cases [0].innerHTML !== '' && cases [0].innerHTML === cases[3].innerHTML && cases [3].innerHTML === cases [6].innerHTML) {
//         afficherGagnant(cases[0].innerHTML);
// }

//     else if (cases [1].innerHTML !== '' && cases [1].innerHTML === cases[4].innerHTML && cases [4].innerHTML === cases [7].innerHTML) {
//         afficherGagnant(cases[1].innerHTML);
// }

//     else if (cases [2].innerHTML !== '' && cases [2].innerHTML === cases[5].innerHTML && cases [5].innerHTML === cases [8].innerHTML) {
//         afficherGagnant(cases[2].innerHTML);
// }

//     //DIAGONALES

//     else if (cases [0].innerHTML !== '' && cases [0].innerHTML === cases[4].innerHTML && cases [4].innerHTML === cases [8].innerHTML) {
//         afficherGagnant(cases[0].innerHTML);
// }

//     else if (cases [2].innerHTML !== '' && cases [2].innerHTML === cases[4].innerHTML && cases [4].innerHTML === cases [6].innerHTML) {
//         afficherGagnant(cases[2].innerHTML);
// }


// }
// function jouer() {

//     if(!this.innerHTML) this.innerHTML = tour();
//         isGagner();
// }
// cases.forEach(el=> el.addEventListener('click', jouer));
